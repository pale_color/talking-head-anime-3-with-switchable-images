# Talking Head Anime from A Single Image 3 with Switchable Images

これは、Pramook Khungurn氏の開発された「Talking Head(?) Anime from A Single Image 3: Now the Body Too」およびそのデモコード実装である「talking-head-anime-3-demo」([https://github.com/pkhungurn/talking-head-anime-3-demo](https://github.com/pkhungurn/talking-head-anime-3-demo)) の派生として作られたソフトウェアプログラムです。

より配信等に活用しやすくなるよう、主にUI周りに変更を施しています。

## 主な変更箇所
* Android用のフェイストラッキングアプリである[MeowFace](https://play.google.com/store/apps/details?id=com.suvidriel.meowface)に対応
* iPhone/Android用のフェイストラッキングアプリである[VTube Studio](https://denchisoft.com/)に対応
* [VMC Protocol](https://protocol.vmc.info/) 経由での動作に対応
* PCでのトラッキングソフトウェアである、[iFacialMocap Powered by NVIDIA Broadcast](https://www.ifacialmocap.com/download/) 、 [ExpressionAppBridge](https://github.com/DrBomb/ExpressionAppBridge)等に対応
* 正面向きの補正に対応（スマートフォンを顔の真正面に置く必要がなくなります）
* 複数の画像の読み込み、切り替えに対応（表情差分などに活用いただけます）
* GPUでの推論処理のスレッドを分離（若干ですがFPS向上が期待されます）
* アニメーション間隔を指定するコマンドラインオプションを追加（フレームレートを制限してGPUの占有を避けることができます）
* モーションデータを受信するポート番号の指定に対応

## 利用規約

* 2024年8月31日の更新にてライセンスを変更しました。

本ソフトウェアはMITライセンスです。
詳細はLICENSEファイル、並びに後述の権利表記をご確認ください。
```
Talking Head Anime from A Single Image 3 with Switchable Images
Copyright (c) 2023 pale_color
```

再配布を伴わない場合の使用の際はクレジット表記は必須ではありませんが、していただけるとモチベーションにつながります。

## 動作環境
実用的な速度での動作には、CUDAの利用できるGPU（RTX2000番台以降を推奨）、並びに対応したバージョンのCUDA Toolkit、PyTorchが必要です。CPUのみの環境でも動作自体は可能ですが、非常に低速（おそらく1FPS未満）です。

また、フェイストラッキングを使用するには以下のいずれかが必要です。
* アプリ [iFacialMocap](https://www.ifacialmocap.com/) およびそれを利用可能なiPhone端末
* アプリ [MeowFace](https://play.google.com/store/apps/details?id=com.suvidriel.meowface) およびそれを利用可能なAndroid端末
* アプリ [VTube Studio](https://denchisoft.com/) およびそれを利用可能なiPhoneまたはAndroid端末
* [VMC Protocol](https://protocol.vmc.info/) (VRM 0.x 形式のアバター操作)に対応したモーションキャプチャアプリ(*1)
* [VMC Protocol](https://protocol.vmc.info/) (Perfect Sync のブレンドシェイプ送信)に対応したモーションキャプチャアプリ(*2)
* [iFacialMocap Powered by NVIDIA Broadcast](https://www.ifacialmocap.com/download/) または [NVIDIA_ARSDK_iFacialMocap](https://github.com/emoto-yasushi/NVIDIA_ARSDK_iFacialMocap) 、およびその実行環境（RTX2000番台以降のGPUが必要）(*3)
* [ExpressionAppBridge](https://github.com/DrBomb/ExpressionAppBridge)  およびその実行環境(*3)

![LOGO_VMC-Protocol](docs/vmpc_logo_128x128.png "Logo - VMC Protocol")

*1 受信ポートの設定に留意してください。次のパラメータに対応しています。[VSeeFace](https://www.vseeface.icu/)、[Tracking World](https://deatrathias.net/TW/) 、[XR Animator](https://github.com/ButzYung/SystemAnimatorOnline)にて動作確認しています。
```
# ボーン（頭部の向き、視線の向き）
/VMC/Ext/Bone/Pos "Head"
/VMC/Ext/Bone/Pos "RightEye"
/VMC/Ext/Bone/Pos "LeftEye"

# ブレンドシェイプ（表情）
# VRM0系
/VMC/Ext/Blend/Val "Fun"
/VMC/Ext/Blend/Val "Surprised"
/VMC/Ext/Blend/Val "A"
/VMC/Ext/Blend/Val "I"
/VMC/Ext/Blend/Val "U"
/VMC/Ext/Blend/Val "E"
/VMC/Ext/Blend/Val "O"
/VMC/Ext/Blend/Val "Blink"
/VMC/Ext/Blend/Val "Blink_L"
/VMC/Ext/Blend/Val "Blink_R"
# VRM1系
/VMC/Ext/Blend/Val "relaxed"
/VMC/Ext/Blend/Val "surprised"
/VMC/Ext/Blend/Val "aa"
/VMC/Ext/Blend/Val "ih"
/VMC/Ext/Blend/Val "ou"
/VMC/Ext/Blend/Val "ee"
/VMC/Ext/Blend/Val "oh"
/VMC/Ext/Blend/Val "blink"
/VMC/Ext/Blend/Val "blinkleft"
/VMC/Ext/Blend/Val "blinkright"
```

*2 受信ポートの設定に留意してください。ARKitで使われる52種のブレンドシェイプ名に対応しています。[Puppetstring](https://ar14.itch.io/puppetstring)にて動作確認しています。

*3 いずれも、「Motion Capture Method」メニューでは「iFacialMocap (PC)」を選択します。

動作には次の各ライブラリが必要です。
* Python >= 3.8
* PyTorch >= 1.11.0 with CUDA support
* SciPY >= 1.7.3
* wxPython >= 4.1.1
* Matplotlib >= 3.5.1

主に、Anaconda(またはMiniconda)環境での利用を想定しています。環境によっては3行目のPyTorchのインストールについて、適したバージョン指定に変更する必要がある場合があります。
```
conda create -n talking-head-anime-3-with-switchable-images python=3.8
conda activate talking-head-anime-3-with-switchable-images
conda install pytorch==1.12.0 torchvision==0.13.0 torchaudio==0.12.0 cudatoolkit=11.6 -c pytorch -c conda-forge
conda install scipy
pip install wxpython
conda install matplotlib
```

## クイックスタート
* 次のコマンドにより実行して下さい。
```
python tha3/app/ifacialmocap_puppeteer.py --model standard_float --timer 20 --port 49983
```

ここで、``--model``は使用する機械学習モデルを指定するコマンドラインオプションです。モデルは4種類が用意されており、``standard_float``, ``separable_float``, ``standard_half``, ``separable_half``が指定できます。いずれも機能はほぼ同じですが、サイズ、RAM使用量、動作速度、負荷、精度などが異なります。試行の上で適したものを使用してください。なお、オプションを指定しない場合はデフォルトで``standard_float``のモデルが使用されます。

``--timer``はアニメーションの間隔を指定するオプションです。値はミリ秒(ms)単位で、5～2000の範囲の整数で指定します。5未満、または2000を超える値を指定した場合はこの範囲に丸められます。なお、オプションを指定しない場合はデフォルトで``20``とみなされます。

``--port``はモーションキャプチャデータの受信を待機するポート番号です。オプションを指定しない場合はデフォルトで``49983``とみなされます。なお、ポート番号については、iFacialMocap、MeowFace、その他iFacialMocap形式の出力を行うソフトウェアでは``49983``が、VTube Studioでは``11125``が、VMC Protocol形式の出力を行うソフトウェアでは``39539``または``39540``が基本となりますので、使うソフトウェアあるいはアプリによって適切な値を設定するようにしてください。

なお、``tha3sw_config.json``の``save_config``が``true``である場合（デフォルト）、終了時に設定を同jsonファイル内に自動保存し、次回起動時に読み込みます。ウィンドウ内での設定項目のほか、上記コマンドラインオプションも記憶され、次回起動時にオプションを指定しなかった場合には保存された設定値が読み込まれます。

* 操作説明

![UI-QUICKSTART](docs/ui-quickstart.png "The guide to start with new UI")

iFacialMocap、MeowFace、あるいはVTube Studioと接続する際には、スマートフォンのIPアドレスを入力した上で「START CAPTURE」を押してください。

その他のソフトウェアと接続する場合は、送信元のソフトウェアでIPアドレスを適切に設定してください。

「START CAPTURE」ボタンの右側に、「Status」として接続状態が表示されます。状態は ○ 部分の色で次の通り表されます。
* 灰色 　接続なし・受信データなし
* 黄色 　接続あり・受信データエラー
* 緑色 　接続あり・受信データ正常

## 実験的機能
次のコマンドにより、1024pixel×1024pixelの大きさの画像の入出力が可能なプログラムを起動できます（``--model``、``--timer``、``--port``の各オプションの使用も可能です）。

（手動）
```
python tha3/app/manual_poser_1024.py
```

（フェイストラッキング）
```
python tha3/app/ifacialmocap_puppeteer_1024.py
```

* 内部では、入力画像を間引くような形で4分割し、推論した出力画像を組み合わせる、といった処理を行っています。そのため、出力にずれやノイズが生じます。
* 分割、合成の仕方が異なる4種類の手法を選択できます。
0. もっともシャープだが網状のノイズが出やすい
1. 中間的（ややシャープ寄り）
2. 中間的（ややソフト寄り）
3. ノイズは目立たないがややぼけが生じる
* 実行にはフルHD以上の解像度を持つディスプレイが必要です。なお、入力画像は512pixel×512pixelにリサイズして表示されます。
* 高負荷なため、特にフェイストラッキング用の``ifacialmocap_puppeteer_1024.py``の場合、ハイエンドのGPUを推奨します。

![UI-ManualPoser-1024pixel](docs/ui-manualposer-1024.PNG "The guide with manual_poser_1024")

![UI-FaceMotionCapture-1024pixel](docs/ui-facemocap-1024.PNG "The guide with ifacialmocap_puppeteer_1024")

## 実験的機能・差分の事前レンダリング
表情差分を事前にレンダリングして作成する方式により、GPUに依存せずに実行可能なものになります。

前述の動作環境に記載のライブラリに加えて、``numba``ライブラリが必要です。``pip``あるいは``conda``などでインストールしてください。

（差分の作成）
```
python tha3/app/prerendering_creator.py
```

``--model``オプションが使用可能です。

``prerendering_creator.py``を起動して「Load Image」ボタンを押して画像ファイルを読み込むと、その画像ファイルのあるディレクトリ下に``（拡張子を除いた画像ファイル名）_tha``というディレクトリが作成され、その中に差分が作成されます。100MB強の空き容量が必要です。なお、ファイルを移動させる際は元の画像ファイルと作成されたディレクトリを必ず一緒に移動するようにしてください。

ウィンドウ右下のメッセージに「Prerendering finished.」と表示されたら差分のレンダリングは終了です。すべての差分の作成には、CUDAが有効な環境の場合は数分程度、CPUのみ有効な環境では数十分程度かかります。途中で終了させた場合、再度同じ画像ファイルを指定して読み込むことで続きから差分の作成を行うことができます。

（フェイストラッキング）
```
python tha3/app/mocap_puppeteer_prerendering.py
```

``--timer``、``--port``オプションが使用可能です。``--model``オプションは使用できません。

``mocap_puppeteer_prerendering.py``を起動して、「Load Image」ボタンを押して差分作成の際の元となった画像ファイルを選択してください。自動的に差分を含めて読み込みが行われます。その他のUIは操作説明の項とほぼ共通です。ただし、「Iris Size」の変更には対応していないため、スライドバーは無効化されています。

## 権利表記
* 本ソフトウェアはMITライセンスでリリースされた「talking-head-anime-3-demo」([https://github.com/pkhungurn/talking-head-anime-3-demo](https://github.com/pkhungurn/talking-head-anime-3-demo))を使用しています。
* data/modelsフォルダ内の学習済みモデルは、クリエイティブコモンズ 表示 4.0 国際 (CC BY 4.0) ライセンスのもとでリリースされています。

> Copyright (c) 2022 Pramook Khungurn

* data/imagesフォルダ内のサンプル画像のライセンスについては同フォルダ内のREADME.mdをご確認ください。

## 変更されていない個所については、以下もご一読ください。
ただし、
* Google Colab用の設定は同梱していません。
* モデルファイルは当プロジェクトのリポジトリ内に配置済みです。

－－以下は元のプロジェクト（talking-head-anime-3-demo）のREADMEとなります－－

# Demo Code for "Talking Head(?) Anime from A Single Image 3: Now the Body Too"

This repository contains demo programs for the [Talking Head(?) Anime from a Single Image 3: Now the Body Too](https://pkhungurn.github.io/talking-head-anime-3/index.html) project. As the name implies, the project allows you to animate anime characters, and you only need a single image of that character to do so. There are two demo programs:

* The ``manual_poser`` lets you manipulate a character's facial expression, head rotation, body rotation, and chest expansion due to breathing through a graphical user interface. 
* ``ifiacialmocap_puppeteer`` lets you transfer your facial motion to an anime character.

## Try the Manual Poser on Google Colab

If you do not have the required hardware (discussed below) or do not want to download the code and set up an environment to run it, click [![this link](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/pkhungurn/talking-head-anime-3-demo/blob/master/colab.ipynb) to try running the manual poser on [Google Colab](https://research.google.com/colaboratory/faq.html).

## Hardware Requirements

Both programs require a recent and powerful Nvidia GPU to run. I could personally ran them at good speed with the Nvidia Titan RTX. However, I think recent high-end gaming GPUs such as the RTX 2080, the RTX 3080, or better would do just as well.

The `ifacialmocap_puppeteer` requires an iOS device that is capable of computing [blend shape parameters](https://developer.apple.com/documentation/arkit/arfaceanchor/2928251-blendshapes) from a video feed. This means that the device must be able to run iOS 11.0 or higher and must have a TrueDepth front-facing camera. (See [this page](https://developer.apple.com/documentation/arkit/content_anchors/tracking_and_visualizing_faces) for more info.) In other words, if you have the iPhone X or something better, you should be all set. Personally, I have used an iPhone 12 mini.

## Software Requirements

### GPU Related Software

Please update your GPU's device driver and install the [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit) that is compatible with your GPU and is newer than the version you will be installing in the next subsection.

### Python Environment

Both ``manual_poser`` and ``ifacialmocap_puppeteer`` are available as desktop applications. To run them, you need to set up an environment for running programs written in the [Python](http://www.python.org) language. The environment needs to have the following software packages:

* Python >= 3.8
* PyTorch >= 1.11.0 with CUDA support
* SciPY >= 1.7.3
* wxPython >= 4.1.1
* Matplotlib >= 3.5.1

One way to do so is to install [Anaconda](https://www.anaconda.com/) and run the following commands in your shell:

```
> conda create -n talking-head-anime-3-demo python=3.8
> conda activate talking-head-anime-3-demo
> conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
> conda install scipy
> pip install wxpython
> conda install matplotlib
```

#### Caveat 1: Do not use Python 3.10 on Windows

As of June 2006, you cannot use [wxPython](https://www.wxpython.org/) with Python 3.10 on Windows. As a result, do not use Python 3.10 until [this bug](https://github.com/wxWidgets/Phoenix/issues/2024) is fixed. This means you should not set ``python=3.10`` in the first ``conda`` command in the listing above.

#### Caveat 2: Adjust versions of Python and CUDA Toolkit as needed

The environment created by the commands above gives you Python version 3.8 and an installation of [PyTorch](http://pytorch.org) that was compiled with CUDA Toolkit version 11.3. This particular setup might not work in the future because you may find that this particular PyTorch package does not work with your new computer. The solution is to:

1. Change the Python version in the first command to a recent one that works for your OS. (That is, do not use 3.10 if you are using Windows.)
2. Change the version of CUDA toolkit in the third command to one that the PyTorch's website says is available. In particular, scroll to the "Install PyTorch" section and use the chooser there to pick the right command for your computer. Use that command to install PyTorch instead of the third command above.

![The command to install PyTorch](docs/pytorch-install-command.png "The command to install PyTorch")

### Jupyter Environment

The ``manual_poser`` is also available as a [Jupyter Nootbook](http://jupyter.org). To run it on your local machines, you also need to install:

* Jupyter Notebook >= 7.3.4
* IPywidgets >= 7.7.0

In some case, you will also need to enable the ``widgetsnbextension`` as well. So, run

```
> jupyter nbextension enable --py widgetsnbextension
```

After installing the above two packages. Using Anaconda, I managed to do the above with the following commands:

```
> conda install -c conda-forge notebook
> conda install -c conda-forge ipywidgets
> jupyter nbextension enable --py widgetsnbextension
```

### Automatic Environment Construction with Anaconda

You can also use Anaconda to download and install all Python packages in one command. Open your shell, change the directory to where you clone the repository, and run:

```
> conda env create -f environment.yml
```

This will create an environment called ``talking-head-anime-3-demo`` containing all the required Python packages.

### iFacialMocap

If you want to use ``ifacialmocap_puppeteer``, you will also need to an iOS software called [iFacialMocap](https://www.ifacialmocap.com/) (a 980 yen purchase in the App Store). You do not need to download the paired application this time. Your iOS and your computer must use the same network. For example, you may connect them to the same wireless router.

## Download the Models

Before running the programs, you need to download the model files from this [Dropbox link](https://www.dropbox.com/s/y7b8jl4n2euv8xe/talking-head-anime-3-models.zip?dl=0) and unzip it to the ``data/models`` folder under the repository's root directory. In the end, the data folder should look like:

```
+ data
  + images
    - crypko_00.png
    - crypko_01.png
        :
    - crypko_07.png
    - lambda_00.png
    - lambda_01.png
  + models
    + separable_float
      - editor.pt
      - eyebrow_decomposer.pt
      - eyebrow_morphing_combiner.pt
      - face_morpher.pt
      - two_algo_face_body_rotator.pt
    + separable_half
      - editor.pt
          :
      - two_algo_face_body_rotator.pt
    + standard_float
      - editor.pt
          :
      - two_algo_face_body_rotator.pt
    + standard_half
      - editor.pt
          :
      - two_algo_face_body_rotator.pt
```

The model files are distributed with the 
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/legalcode), which
means that you can use them for commercial purposes. However, if you distribute them, you must, among other things, say 
that I am the creator.

## Running the `manual_poser` Desktop Application

Open a shell. Change your working directory to the repository's root directory. Then, run:

```
> python tha3/app/manual_poser.py
```

Note that before running the command above, you might have to activate the Python environment that contains the required
packages. If you created an environment using Anaconda as was discussed above, you need to run

```
> conda activate talking-head-anime-3-demo
```

if you have not already activated the environment.

### Choosing System Variant to Use

As noted in the [project's write-up](http://pkhungurn.github.io/talking-head-anime-3/index.html), I created 4 variants of the neural network system. They are called ``standard_float``, ``separable_float``, ``standard_half``, and ``separable_half``. All of them have the same functionalities, but they differ in their sizes, RAM usage, speed, and accuracy. You can specify which variant that the ``manual_poser`` program uses through the ``--model`` command line option.

```
> python tha3/app/manual_poser --model <variant_name>
```

where ``<variant_name>`` must be one of the 4 names above. If no variant is specified, the ``standard_float`` variant (which is the largest, slowest, and most accurate) will be used.

## Running the `manual_poser` Jupyter Notebook

Open a shell. Activate the environment. Change your working directory to the repository's root directory. Then, run:

```
> jupyter notebook
```

A browser window should open. In it, open `manual_poser.ipynb`. Once you have done so, you should see that it has two cells. Run the two cells in order. Then, scroll down to the end of the document, and you'll see the GUI there.

You can choose the system variant to use by changing the ``MODEL_NAME`` variable in the first cell. If you do, you will need to rerun both cells in order for the variant to be loaded and the GUI to be properly updated to use it.

## Running the `ifacialmocap_poser`

First, run iFacialMocap on your iOS device. It should show you the device's IP address. Jot it down. Keep the app open.

![IP address in iFacialMocap screen](docs/ifacialmocap_ip.jpg "IP address in iFacialMocap screen")

Open a shell. Activate the Python environment. Change your working directory to the repository's root directory. Then, run:

```
> python tha3/app/ifacialmocap_puppeteer.py
```

You will see a text box with label "Capture Device IP." Write the iOS device's IP address that you jotted down there.

![Write IP address of your iOS device in the 'Capture Device IP' text box.](docs/ifacialmocap_puppeteer_ip_address_box.png "Write IP address of your iOS device in the 'Capture Device IP' text box.")

Click the "START CAPTURE!" button to the right.

![Click the 'START CAPTURE!' button.](docs/ifacialmocap_puppeteer_click_start_capture.png "Click the 'START CAPTURE!' button.")

If the programs are connected properly, you should see the numbers in the bottom part of the window change when you move your head.

![The numbers in the bottom part of the window should change when you move your head.](docs/ifacialmocap_puppeteer_numbers.png "The numbers in the bottom part of the window should change when you move your head.")

Now, you can load an image of a character, and it should follow your facial movement.

## Contraints on Input Images

In order for the system to work well, the input image must obey the following constraints:

* It should be of resolution 512 x 512. (If the demo programs receives an input image of any other size, they will resize the image to this resolution and also outptu at this resolution.)
* It must have an alpha channel.
* It must contain only one humanoid character.
* The character should be standing upright and facing forward.
* The character's hands should be below and far from the head.
* The head of the character should roughly be contained in the 128 x 128 box in the middle of the top half of the image.
* The alpha channels of all pixels that do not belong to the character (i.e., background pixels) must be 0.

![An example of an image that conforms to the above criteria](docs/input_spec.png "An example of an image that conforms to the above criteria")

See the project's [write-up](http://pkhungurn.github.io/talking-head-anime-3/full.html#sec:problem-spec) for more details on the input image.

## Citation

If your academic work benefits from the code in this repository, please cite the project's web page as follows:

> Pramook Khungurn. **Talking Head(?) Anime from a Single Image 3: Now the Body Too.** http://pkhungurn.github.io/talking-head-anime-3/, 2022. Accessed: YYYY-MM-DD.

You can also used the following BibTex entry:

```
@misc{Khungurn:2022,
    author = {Pramook Khungurn},
    title = {Talking Head(?) Anime from a Single Image 3: Now the Body Too},
    howpublished = {\url{http://pkhungurn.github.io/talking-head-anime-3/}},
    year = 2022,
    note = {Accessed: YYYY-MM-DD},
}
```

## Disclaimer

While the author is an employee of [Google Japan](https://careers.google.com/locations/tokyo/), this software is not Google's product and is not supported by Google.

The copyright of this software belongs to me as I have requested it using the [IARC process](https://opensource.google/documentation/reference/releasing#iarc). However, Google might claim the rights to the intellectual
property of this invention.

The code is released under the [MIT license](https://github.com/pkhungurn/talking-head-anime-2-demo/blob/master/LICENSE).
The model is released under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/legalcode). Please see the README.md file in the ``data/images`` directory for the licenses for the images there.